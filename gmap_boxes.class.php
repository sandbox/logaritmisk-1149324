<?php
/**
 * @file
 */


/**
 *
 */
class gmap_boxes extends boxes_box {

  /**
   * Implementation of boxes_box::options_defaults().
   */
  public function options_defaults() {
    return array(
      'markertext' => array(
        'value' => '',
        'format' => filter_default_format(),
      ),
    ) + gmap_defaults();
  }

  /**
   * Implementation of boxes_box::options_form().
   */
  public function options_form(&$form_state) {
    $form = array();

    $form['gmap_settings'] = array(
      '#type' => 'fieldset',
      '#title' => t('Map settings'),
    );

    $form['gmap_settings']['width'] = array(
      '#type' => 'textfield',
      '#title' => t('Width'),
      '#default_value' => $this->options['width'],
      '#size' => 25,
      '#maxlength' => 25,
      '#description' => t('The width of the Google map, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>'),
    );
    $form['gmap_settings']['height'] = array(
      '#type' => 'gmap_dimension',
      '#title' => t('Height'),
      '#default_value' => $this->options['height'],
      '#size' => 25,
      '#maxlength' => 25,
      '#description' => t('The height of the Google map, as a CSS length or percentage. Examples: <em>50px</em>, <em>5em</em>, <em>2.5in</em>, <em>95%</em>'),
    );


    // marker
    $form['gmap_settings']['markermode'] = array(
      '#type' => 'radios',
      '#title' => t('Marker action'),
      '#description' => t('Perform this action when a marker is clicked.'),
      '#options' => array(
        t('Do nothing'),
        t('Open info window'),
        t('Open link')
      ),
      '#default_value' => isset($defaults['markermode']) ? $defaults['markermode'] : 0,
    );

    $form['gmap_settings']['markertext'] = array(
      '#type' => 'text_format',
      '#base_type' => 'textarea',
      '#title' => t('Marker text'),
      '#default_value' => $this->options['markertext']['value'],
      '#rows' => 5,
      '#format' => $this->options['markertext']['format'] ? $this->options['markertext']['format'] : NULL,
    );


    //
    $form['gmap_settings']['latlong'] = array(
      '#type' => 'gmap_latlon',
      '#title' => t('Center'),
      '#default_value' => $this->options['latlong'],
      '#size' => 50,
      '#maxlength' => 255,
      '#description' => t('The center coordinates of Google map, expressed as a decimal latitude and longitude, separated by a comma.'),
    );

    $form['gmap_settings']['zoom'] = array(
      '#type' => 'select',
      '#title' => t('Initial zoom'),
      '#default_value' => $this->options['zoom'],
      '#options' => array('auto' => 'auto') + drupal_map_assoc(range(0, 17)),
      '#description' => t('The default zoom level of a Google map.'),
    );

    $form['gmap_settings']['map']['controltype'] = array(
      '#type' => 'select',
      '#title' => t('Control type'),
      '#options' => array(
        'None' => t('None'),
        'Micro' => t('Micro'),
        'Small' => t('Small'),
        'Large' => t('Large')
      ),
      '#default_value' => $this->options['controltype'],
    );

    $form['gmap_settings']['map']['mtc'] = array(
      '#type' => 'select',
      '#title' => t('Map Type Control'),
      '#options' => array(
        'none' => t('None'),
        'standard' => t('Standard (GMapTypeControl)'),
        'hier' => t('Hierarchical (GHierarchicalMapTypeControl)'),
        'menu' => t('Dropdown (GMenuMapTypeControl)'),
      ),
      '#default_value' => $this->options['mtc'],
    );


    // baselayers
    $form['gmap_settings']['baselayers'] = array(
      '#type' => 'fieldset',
      '#title' => t('Enabled map types ("base layers")'),
    );

    $baselayers = array();

    gmap_module_invoke('baselayers', $baselayers);

    $options = array();
    foreach ($baselayers as $name => $layers) {
      $options[$name] = array();
      foreach ($layers as $k => $v) {
        $options[$name][$k] = $v['title'];
      }
    }

    $form['gmap_settings']['baselayers']['maptype'] = array(
      '#type' => 'select',
      '#tree' => FALSE,
      '#parents' => array('gmap_default', 'maptype'),
      '#title' => t('Default map type'),
      '#default_value' => $this->options['maptype'],
      '#options' => $options,
    );

    foreach ($baselayers as $name => $layers) {
      $form['gmap_settings']['baselayers'][$name] = array(
        '#type' => 'fieldset',
        '#title' => $name,
      );
      foreach ($layers as $key => $layer) {
        $form['gmap_settings']['baselayers'][$name][$key] = array(
          '#type' => 'checkbox',
          '#title' => $layer['title'],
          '#parents' => array('gmap_default', 'baselayers', $key),

          '#description' => $layer['help'],
          '#default_value' => isset($this->options['baselayers'][$key]) ? $this->options['baselayers'][$key] : $layer['default'],
        );
      }
    }


    // behavior
    $form['gmap_settings']['behavior'] = array(
      '#type' => 'fieldset',
      '#title' => t('Map Behavior flags'),
      '#tree' => TRUE,
      '#description' => t('Behavior flags modify how a map behaves. Grayed out flags are not settable here, but may be set on a map by map basis via code or a macro. Changes to behaviors will not affect the preview map shown above until changes are saved.'),
    );
    $m = array();
    $behaviors = gmap_module_invoke('behaviors', $m);
    foreach ($behaviors as $k => $v) {
      $form['gmap_settings']['behavior'][$k] = array(
        '#type' => 'checkbox',
        '#title' => t('@name : @title', array('@name' => $k, '@title' => $v['title'])),
        '#default_value' => isset($this->options['behavior'][$k]) ? $this->options['behavior'][$k] : $v['default'],
        '#description' => isset($v['help']) ? $v['help'] : '',
      );
      if (isset($v['internal']) && $v['internal']) {
        $form['gmap_settings']['behavior'][$k]['#disabled'] = TRUE;
        $form['gmap_settings']['behavior'][$k]['#value'] = $v['default'];
      }
    }


    return $form;
  }

  /**
   * Implementation of boxes_box::render().
   */
  public function render() {
    $title = isset($this->title) ? check_plain($this->title) : NULL;

    list($latitude, $longitude) = explode(',', $this->options['latlong']);

    $settings = array(
      'id' => gmap_get_auto_mapid(),
      'latitude' => $latitude,
      'longitude' => $longitude,
      'zoom' => $this->options['zoom'],
      'width' => $this->options['width'],
      'height' => $this->options['height'],
      'markers' => array(
        array(
          'latitude' => $latitude,
          'longitude' => $longitude,
          'markername' => '',
          'offset' => 0,
          'text' => check_markup($this->options['markertext']['value'], $this->options['markertext']['format'], $langcode = '', FALSE),
          'autoclick' => FALSE,
        ),
      ),
    );

    $settings = array_merge($settings, $this->options);

    $element = array(
      '#type' => 'gmap',
      '#gmap_settings' => $settings,
    );

    $content = drupal_render($element);

    return array(
      'delta' => $this->delta,
      'title' => $title,
      'subject' => $title,
      'content' => $content,
    );
  }

}
